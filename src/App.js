
import './App.css';
import image from "./assets/images/48.jpeg"

function App() {
  return (
    <div className='wrapper'>
      <img src={image} className="avatar" alt="avatar"></img>
      <p className='quote'>This is the one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p className='name'><b>Tammy Stevens </b>Front End Developer</p>
    </div>
  );
}

export default App;
